print("Import des librairies et chargement du modèle de langue...")

from bs4 import BeautifulSoup as bs
import re, spacy
# nlp = spacy.load("fr_core_news_sm")


print("Chargement des fichiers...")

fs = 'g1jpfr/g1jpfr_source.xml'
fi = 'g1jpfr/g1jpfr_initial.xml'
fr = 'g1jpfr/g1jpfr_revision.xml'
ff = 'g1jpfr/g1jpfr_final.xml'

with open(fs,'r') as f:
    xs = bs(f.read(), "xml")

with open(fi,'r') as f:
    xi = bs(f.read(), "xml")

with open(fr,'r') as f:
    xr = bs(f.read(), "xml")

with open(ff,'r') as f:
    xf = bs(f.read(), "xml")


xrPliste = xr.find_all('P')


######### Code pour récupérer les énoncés entiers #########

for x,p in enumerate(xrPliste):
    # RECUPERER LA VERSION INITIALE ET LA VERSION FINALE
    
    xrPini = re.sub(r'<edit auteur="[^"]*" categorie="correction" comment="[^"]*" date="[^"]*" final="[^"]*" idodf="([^"]*)" initial="([^"]*)" resolu="1" tags="" type="[^"]*"/>', r'°{\1}\2*', str(xrPliste[x]))
    xrPfin = re.sub(r'<edit auteur="[^"]*" categorie="correction" comment="[^"]*" date="[^"]*" final="([^"]*)" idodf="([^"]*)" initial="[^"]*" resolu="1" tags="" type="[^"]*"/>', r'°{\2}\1*', str(xrPliste[x]))
    xrPini = re.sub(r'<P id="\w+"/?>','', xrPini)
    xrPini = re.sub(r'</P>','', xrPini)
    xrPfin = re.sub(r'<P id="\w+"/?>','', xrPfin)
    xrPfin = re.sub(r'</P>','', xrPfin)
    
    # nlpIni = nlp(xrPini)
    # nlpFin = nlp(xrPfin)

    def getListEditPos(xrp):
        listEditPos = []
        cptCar = 0

        for x, car in enumerate(xrp):
            if car == "°":
                listEditPos.append({
                    "idodf": xrp[x+2:x+18],
                    "debut": cptCar,
                    "fin": None
                })
                xrp = xrp[:cptCar] + xrp[cptCar+19:] # suppression de l'ancre et de l'identifiant
            elif car == "*":
                listEditPos[-1]["fin"] = cptCar
                xrp = xrp[:cptCar] + xrp[cptCar+1:] # suppression de l'ancre
            elif car != " ":
                cptCar += 1
    
        return listEditPos, xrp

    print('\nP',x)
    print('\t', xrPini)
    listEditPos, xrp = getListEditPos(xrPini)
    print('\t', xrp)
    for e in listEditPos:
        print(e)
    
    # cpt = 0
    # for token in xrPini:
    #     # on incrémente à chaque caractère sauf ' '
    #     for car in token.text:
    #         if cpt >= listEditPos[0]['debut'] and cpt <= listEditPos[0]['fin']:

    #         cpt += 1 if car != ' '


    # print('\t', xrPfin)


"""

x1 = ""
x2 = ""

# # Décommenter pour visualiser le chemin
# x1 = "^"
# x2 = "*"

def checkBefore(edit, etat):
    avant = ""

    if edit.previous_sibling != None:
        elgauche = edit.previous_sibling

        if str(elgauche)[:5] == "<edit":
            # si elgauche est une balise edit
            if re.match(r'^\w+$|^’\w*$|^$', str(elgauche[etat])):
                # si etat ne commence pas par [ .,]
                avant += x2 + checkBefore(elgauche, etat) + x2
                avant +=  x1 + elgauche[etat] + x1

        elif str(elgauche)[-1] != ">":
            # si elgauche est une string
            if re.match(r'^\w+$|^’\w*$|^$', str(elgauche)):
                # si la string ne contient qu'un mot sans espace, peut-être qu'il n'est pas entier: checkBefore()
                avant += x2 + checkBefore(elgauche, etat) + x2

            checkword = re.match(r".*?(\w+|\w*’)$", str(elgauche))
            if checkword:
                avant += x1 + checkword.group(1) + x1

    return avant


def checkAfter(edit, etat):
    apres = ""
    
    if edit.next_sibling != None:
        eldroit = edit.next_sibling

        if str(eldroit)[:5] == "<edit":
            # si eldroit est une balise edit
            if re.match(r'^\w+$|^’\w*$|^$', str(eldroit[etat])):
                # si etat ne commence pas par [ .,]
                firstWord = re.match(r'^(’\w*|\w+).*?$', str(eldroit[etat]))
                tok = firstWord.group(1) if firstWord else ""
                apres +=  x1 + tok + x1
                apres += x2 + checkAfter(eldroit, etat) + x2

        elif str(eldroit)[-1] != ">":
            # si eldroit est une string
            checkword = re.match(r"^(’\w*|\w+).*?$", str(eldroit))
            if checkword:
                apres += x1 + checkword.group(1) + x1

            if re.match(r'^\w+$|^’\w*$|^$', str(eldroit)):
                # si la string ne contient qu'un mot sans espace, peut-être qu'il n'est pas entier: checkAfter()
                apres += x2 + checkAfter(eldroit, etat) + x2

    return apres


def recupContext(edit, etat):
    if len(edit[etat]) > 0:
        # ETAT INITIAL DE LA TRADUCTION
        avant = ""
        apres = ""
        #print("|{}|".format(edit[etat]))
        if not re.match(r'^[ .,].*$', str(edit[etat])):
            avant = checkBefore(edit, etat)
        
        if not re.match(r'^.*[ .,]$', str(edit[etat])):
            apres = checkAfter(edit, etat)

        contexte = avant + edit[etat] + apres
        txt = nlp(contexte)
        tags = []
        for token in txt:
            tags.append({
                "text": token.text,
                "lemma": token.lemma_,
                "pos": token.pos_,
                "tag": token.tag_,
                "dep": token.dep_
            })

        #print("\033[0;35m{}\033[0m|{}|\033[0;35m{}\033[0m".format(avant, edit[etat], apres))
        return edit['idodf'], avant, edit[etat], apres, tags


##### AFFICHER LA LISTE :    id  +  contexte.texte.contexte + tags[{},]
# print("Affichage du résultat :")
# for x,r in enumerate(xrPliste):
#     print('\nP',x)
#     edits = xrPliste[x].find_all('edit')

#     for edit in edits:
#         data = recupContext(edit,'initial')
#         if data:
#             idodf, avant, contenu, apres, tags = data
#             print("{}:\t\033[0;35m{}\033[0m{}\033[0;35m{}\033[0m\tTAGS:{}".format(idodf, avant, contenu, apres, tags))
        
#         data = recupContext(edit,'final')
#         if data:
#             idodf, avant, contenu, apres, tags = data
#             print("{}:\t\033[0;35m{}\033[0m{}\033[0;35m{}\033[0m\tTAGS:{}".format(idodf, avant, contenu, apres, tags))


# ##### REMPLIR LES TAGS ET EXPORTER XML
print("Affichage du résultat :")
for x,r in enumerate(xrPliste):
    print('\nP',x)
    edits = xrPliste[x].find_all('edit')

    for edit in edits:
        data = recupContext(edit,'initial')
        if data:
            idodf, avant, contenu, apres, tags = data
            xr.find(idodf=idodf)['tags'] = str(tags)
        
        data = recupContext(edit,'final')
        if data:
            idodf, avant, contenu, apres, tags = data
            xr.find(idodf=idodf)['tags'] = str(tags)

print("Export...")
with open('g1jpfr_revision_tags.xml', 'w') as outFile:
    outFile.write(str(xr))
print('Terminé.')


"""