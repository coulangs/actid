from bs4 import BeautifulSoup as bs

fs = 'g1jpfr/g1jpfr_source.xml'
fi = 'g1jpfr/g1jpfr_initial.xml'
fr = 'g1jpfr/g1jpfr_revision.xml'
ff = 'g1jpfr/g1jpfr_final.xml'

with open(fs,'r') as f:
    xs = bs(f.read(), "xml")

with open(fi,'r') as f:
    xi = bs(f.read(), "xml")

with open(fr,'r') as f:
    xr = bs(f.read(), "xml")

with open(ff,'r') as f:
    xf = bs(f.read(), "xml")

q = False
while not q:
    req = input()
    if req != "q":
        print(xs.find('P', id=req))
        print(xi.find('P', id=req))
        print(xr.find('P', id=req))
        print(xf.find('P', id=req))
    else:
        q = True

'''

items = mydoc.getElementsByTagName('item')

# one specific item attribute
print('Item #2 attribute:')
print(items[1].attributes['name'].value)

# all item attributes
print('\nAll attributes:')
for elem in items:
    print(elem.attributes['name'].value)

# one specific item's data
print('\nItem #2 data:')
print(items[1].firstChild.data)
print(items[1].childNodes[0].data)

# all items data
print('\nAll item data:')
for elem in items:
    print(elem.firstChild.data)
'''