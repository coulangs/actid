from bs4 import BeautifulSoup as bs
import re

import MeCab
chasen = MeCab.Tagger("-Ochasen")
# print(chasen.parse("太郎は海外に住み始めました"))

fs = 'g1frjp/g1frjp_source.xml'
fi = 'g1frjp/g1frjp_initial.xml'
fr = 'g1frjp/g1frjp_revision.xml'
ff = 'g1frjp/g1frjp_final.xml'

with open(fr,'r') as f:
    xr = bs(f.read(), "xml")

xrPliste = xr.find_all('P')


x1 = ""
x2 = ""

# # Décommenter pour visualiser le chemin
# x1 = "^"
# x2 = "*"

def checkBefore(edit, etat):
    avant = ""

    if edit.previous_sibling != None:
        elgauche = edit.previous_sibling

        if str(elgauche)[:5] == "<edit":
            # si elgauche est une balise edit
            if re.match(r'^\w+$|^’\w*$|^$', str(elgauche[etat])):
                # si etat ne commence pas par [ .,]
                avant += x2 + checkBefore(elgauche, etat) + x2
                avant +=  x1 + elgauche[etat] + x1

        elif str(elgauche)[-1] != ">":
            # si elgauche est une string
            if re.match(r'^\w+$|^’\w*$|^$', str(elgauche)):
                # si la string ne contient qu'un mot sans espace, peut-être qu'il n'est pas entier: checkBefore()
                avant += x2 + checkBefore(elgauche, etat) + x2

            checkword = re.match(r".*?(\w+|\w*’)$", str(elgauche))
            if checkword:
                avant += x1 + checkword.group(1) + x1

    return avant


def checkAfter(edit, etat):
    apres = ""
    
    if edit.next_sibling != None:
        eldroit = edit.next_sibling

        if str(eldroit)[:5] == "<edit":
            # si eldroit est une balise edit
            if re.match(r'^\w+$|^’\w*$|^$', str(eldroit[etat])):
                # si etat ne commence pas par [ .,]
                firstWord = re.match(r'^(’\w*|\w+).*?$', str(eldroit[etat]))
                tok = firstWord.group(1) if firstWord else ""
                apres +=  x1 + tok + x1
                apres += x2 + checkAfter(eldroit, etat) + x2

        elif str(eldroit)[-1] != ">":
            # si eldroit est une string
            checkword = re.match(r"^(’\w*|\w+).*?$", str(eldroit))
            if checkword:
                apres += x1 + checkword.group(1) + x1

            if re.match(r'^\w+$|^’\w*$|^$', str(eldroit)):
                # si la string ne contient qu'un mot sans espace, peut-être qu'il n'est pas entier: checkAfter()
                apres += x2 + checkAfter(eldroit, etat) + x2

    return apres


def recupContext(edit, etat):
    if len(edit[etat]) > 0:
        # ETAT INITIAL DE LA TRADUCTION
        avant = ""
        apres = ""
        #print("|{}|".format(edit[etat]))
        if not re.match(r'^[ .,].*$', str(edit[etat])):
            avant = checkBefore(edit, etat)
        
        if not re.match(r'^.*[ .,]$', str(edit[etat])):
            apres = checkAfter(edit, etat)

        # contexte = avant + edit[etat] + apres

        txt = chasen.parse(edit[etat])
        txt = txt.split('\n')
        tags = []
        for token in txt:
            if token != "EOS" and len(token) > 0:
                token = token.split('\t')
                if token not in tags:
                    tags.append({
                        "text": token[0],
                        "lemma": token[2],
                        "pos": token[3].split('-')[0].strip(),
                        "tag": " ".join(token[3:]).strip(),
                        "dep": ""
                    })

        #print("\033[0;35m{}\033[0m|{}|\033[0;35m{}\033[0m".format(avant, edit[etat], apres))
        return edit['idodf'], avant, edit[etat], apres, tags

##### AFFICHER LA LISTE :    id  +  contexte.texte.contexte + tags[{},]
# print("Affichage du résultat :")
# for x,r in enumerate(xrPliste):
#     print('\nP',x)
#     edits = xrPliste[x].find_all('edit')

#     for edit in edits:
#         data = recupContext(edit,'initial')
#         if data:
#             idodf, avant, contenu, apres, tags = data
#             print("{}:\t\033[0;35m{}\033[0m{}\033[0;35m{}\033[0m\tTAGS:{}".format(idodf, '', contenu, '', tags))
        
#         data = recupContext(edit,'final')
#         if data:
#             idodf, avant, contenu, apres, tags = data
#             print("{}:\t\033[0;35m{}\033[0m{}\033[0;35m{}\033[0m\tTAGS:{}".format(idodf, '', contenu, '', tags))


# ##### REMPLIR LES TAGS ET EXPORTER XML
print("Affichage du résultat :")
for x,r in enumerate(xrPliste):
    print('\nP',x)
    edits = xrPliste[x].find_all('edit')

    for edit in edits:
        data = recupContext(edit,'initial')
        thisEditTags = []
        if data:
            idodf, avant, contenu, apres, tags = data
            thisEditTags = tags
        
        data = recupContext(edit,'final')
        if data:
            idodf, avant, contenu, apres, tags = data
            for t in tags:
                if t not in thisEditTags:
                    thisEditTags.append(t)
        
        xr.find(idodf=idodf)['tags'] = str(thisEditTags)

print("Export...")
with open('g1frjp_revision_tags.xml', 'w') as outFile:
    outFile.write(str(xr))
print('Terminé.')


######### Code pour récupérer les énoncés entiers #########
'''
for x,p in enumerate(xrPliste):
    # # RECUPERER LA VERSION INITIALE ET LA VERSION FINALE
    # xrPini = re.sub(r'<edit auteur="[^"]*" categorie="correction" comment="[^"]*" date="[^"]*" final="[^"]*" idodf="[^"]*" initial="([^"]*)" resolu="1" tags="" type="[^"]*"/>', r'\1', str(xrPliste[i]))
    # xrPfin = re.sub(r'<edit auteur="[^"]*" categorie="correction" comment="[^"]*" date="[^"]*" final="([^"]*)" idodf="[^"]*" initial="[^"]*" resolu="1" tags="" type="[^"]*"/>', r'\1', str(xrPliste[i]))
    # xrPini = re.sub(r'<P id="\w+">','', xrPini)
    # xrPini = re.sub(r'</P>','', xrPini)
    # xrPfin = re.sub(r'<P id="\w+">','', xrPfin)
    # xrPfin = re.sub(r'</P>','', xrPfin)

    # Récupérer le mot entier concerné par l'annotation (contexte gauche, contexte droit si il s'agit du même mot)
    # ex. Un avocat ('', 'Un a', 'vocat') ; dires ('di', 're', 's')
    iniliste = re.findall(r'(\w+)?(<edit auteur="[^"]*" categorie="correction" comment="[^"]*" date="[^"]*" final="[^"]*" idodf="[^"]*" initial="[^"]*" resolu="1" tags="" type="[^"]*"/>)?<edit auteur="[^"]*" categorie="correction" comment="[^"]*" date="[^"]*" final="[^"]*" idodf="([^"]*)" initial="([^"]+)" resolu="1" tags="" type="[^"]*"/>(<edit auteur="[^"]*" categorie="correction" comment="[^"]*" date="[^"]*" final="[^"]*" idodf="[^"]*" initial="[^"]*" resolu="1" tags="[^"]*" type="[^"]*"/>)?(\w+)?', str(p))
    finliste = re.findall(r'(\w+)?(<edit auteur="[^"]*" categorie="correction" comment="[^"]*" date="[^"]*" final="[^"]*" idodf="[^"]*" initial="[^"]*" resolu="1" tags="" type="[^"]*"/>)?<edit auteur="[^"]*" categorie="correction" comment="[^"]*" date="[^"]*" final="([^"]+)" idodf="([^"]*)" initial="[^"]*" resolu="1" tags="" type="[^"]*"/>(<edit auteur="[^"]*" categorie="correction" comment="[^"]*" date="[^"]*" final="[^"]*" idodf="[^"]*" initial="[^"]*" resolu="1" tags="[^"]*" type="[^"]*"/>)?(\w+)?', str(p))

    # print('\nP',x)

    # print("INITIAL")
    for i in iniliste:
        exp = ''.join((i[0],i[3],i[5]))
        # print(exp, '\t',(i[0], i[3], i[5]))

        # anaExp = nlp(exp)
        # tags = []
        # for tok in anaExp:
        #     tags.append((tok.pos_,tok.tag_))
        
        # print(i[2], '\t', exp, '\t', tags)

    # print("\nFINAL")
    for i in finliste:
        exp = ''.join((i[0],i[2],i[5]))
        # print(exp, '\t',(i[0], i[2], i[5]))

        # anaExp = nlp(exp)
        # tags = []
        # for tok in anaExp:
        #     tags.append((tok.pos_,tok.tag_))

        # print(i[3], '\t', exp, '\t', tags)
'''