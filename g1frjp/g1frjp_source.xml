<?xml version="1.0" encoding="UTF-8"?>
<DOCUMENT id="g1frjp_source">
    <TITRE id="titre0">
		<P id="p0">Réseaux sociaux et bonheur, un duo incompatible ?</P>
	</TITRE>

    <TEXTE>
        <PAR id="par1" type="chapeau">
            <P id="p1">Aux États-Unis, le sentiment de bonheur ne cesse de dégringoler depuis les années 2000.</P>
			<P id="p2">Le déclin du capital social, l’obésité et la toxicomanie expliquent en partie ce phénomène.</P>
			<P id="p3">Mais la manière dont les adolescents américains passent leur temps libre a également un rôle à jouer.</P>
			<P id="p4">Et les réseaux sociaux représentent une grande partie du problème.</P>
        </PAR>
        <PAR id="par2">
            <P id="p5">Essayez de taper « bonheur » dans un moteur de recherche ou une banque d’images, vous ne trouverez aucune illustration de quelqu’un qui se fend la poire tout seul devant son smartphone.</P>
			<P id="p6">On y trouve plutôt des gens qui sourient, entre amis, en couple ou en famille.</P>
			<P id="p7">Il y a des couleurs partout, du soleil, parfois une touche de mélancolie, des ballons colorés et des animaux mignons.</P>
			<P id="p8">Le monde des bisounours en plein.</P>
			<P id="p9">Certes, on peut se marrer tout seul devant son écran d’ordinateur parce qu’on se mate la vidéo qui compile les plus belles gamelles de gens inconnus.</P>
			<P id="p10">Cela s’appelle un moment de joie.</P>
			<P id="p11">Mais est-ce que cela est suffisant pour être véritablement heureux ?</P>
			<P id="p12">Ce serait déjà beaucoup plus sympa si on montrait la vidéo à quelqu’un et qu’on rigolait ensemble.</P>
			<P id="p13">On ne peut s’empêcher de penser à la phrase gribouillée au détour d’un livre par Christopher McCandless dans le film Into the wild et reprise sur nombreux bancs d’écoles et légendes de photos de profil Facebook : « Happiness is only real when shared ».</P>
            <P id="p14">Mais une bande d’adolescents qui a le nez scotché à leur écran ensemble, peuvent-ils développer des relations qui les rendront heureux ?</P>
			<P id="p15">Ils vous diront que oui.</P>
			<P id="p16">Pourtant plusieurs études ont tiré des conclusions plutôt interpellantes à ce sujet : les réseaux sociaux rendent nos adolescents (parce que c’est en général sur eux que les études ont été faites) malheureux.</P>
			<P id="p17">Alors que l’inverse n’est pas forcément vrai.</P>
			<P id="p18">Finalement, il est où le bonheur ?</P>
			<P id="p19">Vous avez deux heures.</P>
        </PAR>
        <TITRE id="titre1" niveau="2">
            <P id="p20">Davantage de mal-être</P>
        </TITRE>
        <PAR id="par3">
            <P id="p21">Une enquête générale relayée par un rapport sur le bonheur dans le monde publié à l’occasion de la journée mondiale du bonheur le 20 mars dernier met en évidence le bonheur déclinant des adultes américains depuis les années 2000 et particulièrement depuis la fin de la grande récession en 2009.</P>
			<P id="p22">Du côté des adolescents, c’est après 2012 que le bonheur a commencé à décliner pour finalement tomber d’accord vers 2016, moment auquel les adultes et les adolescents « ont signalé beaucoup moins de bonheur que dans les années 2000 », rapporte l’étude.</P>
			<P id="p23">Dans le même temps, les indicateurs du bien-être psychologique sont en chute libre.</P>
			<P id="p24">Dépression, idées suicidaires, automutilation ont fortement augmenté chez les adolescents depuis 2010.</P>
			<P id="p25">« En particulier chez les filles et les jeunes femmes ».</P>
        </PAR>
        <PAR id="par4">
            <P id="p26">Pourtant, ces jeunes américains « devraient être plus heureux que jamais : le taux de crimes violents est faible, tout comme le taux de chômage. Le revenu par habitant a régulièrement augmenté au cours des dernières décennies.</P>
			<P id="p27">C’est le paradoxe d’Easterlin : à mesure que le niveau de vie s’améliore, le bonheur devrait en faire autant – mais ce n’est pas le cas », explique Jean M. Twenge, professeur de psychologie à l’université de San Diego et directeur de plusieurs études sur le sujet.</P>
			<P id="p28">Bien évidemment, les réseaux sociaux ne sont pas les seuls responsables d’un certain déclin du bonheur chez les adolescents : le déclin du capital social et du soutien social, l’augmentation de l’obésité et de la toxicomanie sont des facteurs à prendre en compte, d’après le professeur Jeffrey Sachs.</P>
			<P id="p29">Le professeur Twenge le reconnaît mais va plus loin.</P>
			<P id="p30">« Dans cet article, je suggère une autre explication complémentaire : les Américains sont moins heureux en raison de changements fondamentaux dans la manière dont ils consacrent leurs loisirs.</P>
			<P id="p31">Je me concentre principalement sur les adolescents, car des analyses plus approfondies sur les tendances de l’emploi du temps ont été réalisées pour ce groupe d’âge.</P>
			<P id="p32">Cependant, les analyses futures pourraient révéler que des tendances similaires apparaissent également chez les adultes ».</P>
        </PAR>
        <TITRE id="titre2" niveau="2">
            <P id="p33">Vous faites quoi de vos temps libres ?</P>
        </TITRE>
        <PAR id="par5">
            <P id="p34">L’année passée, un pareil rapport s’était déjà penché sur le sujet.</P>
			<P id="p35">Cette année, c’est la seconde partie de l’étude qui a été dévoilée au sujet du bonheur dans le monde, installant pour la deuxième année consécutive la Finlande sur la première marche du podium des pays les plus heureux du monde (suivi par le Danemark et la Norvège).</P>
			<P id="p36">Il en ressort que les adolescents qui passent plus de temps devant un écran (qu’il s’agisse de jeux vidéos, de réseaux sociaux ou de sms) sont moins heureux que ceux qui font du sport, lisent ou ont des interactions sociales en face à face.</P>
        </PAR>
        <PAR id="par6">
            <P id="p37">Le temps consacré par les adolescents aux écrans s’est accéléré depuis 2012.</P>
			<P id="p38">En 2017, les adolescents de 17 et 18 ans « consacraient en moyenne plus de six heures par jour à seulement trois activités de médias numériques : internet, réseaux sociaux et textos ».</P>
            <P id="p39">En 2018, 45% des adolescents américains disaient être en ligne « presque tout le temps », selon une autre étude.</P>
			<P id="p40">Résultat, on a beau dire ce qu’on veut, pendant qu’ils ont les yeux rivés à leur écran, les ados n’interagissent pas en direct avec d’autres personnes.</P>
			<P id="p41">Et c’est ainsi que les habitudes évoluent.</P>
			<P id="p42">En un peu plus de 30 ans, le temps passé par les jeunes pour se socialiser en face à face a diminué d’une heure par jour.</P>
			<P id="p43">Et toutes les activités qui ne comprennent pas un écran ont perdu leur place dans le cœur des adolescents : ils lisent moins, dorment moins, ne vont plus à la messe, négligent leurs devoirs et les activités extrascolaires.</P>
			<P id="p44">« Le temps passé en ligne par les adolescents a augmenté proportionnellement à la diminution du temps de sommeil et les interactions sociales en personne, le tout lié à la diminution du bonheur général ».</P>
        </PAR>
        <PAR id="par7">
            <P id="p45">« En d’autres termes, les médias numériques peuvent avoir un effet indirect sur le bonheur, car ils prennent la place de temps qui pourrait autrement être consacré à des activités plus bénéfiques ».</P>
			<P id="p46">Sentiment d’absence de bonheur et réseaux sociaux sont donc liés même si le lien de cause à effet devrait encore faire l’objet d’études plus poussées avant d’être complètement confirmé.</P>
			<P id="p47">En effet, est-ce que les gens sont malheureux parce qu’ils passent trop de temps sur les réseaux sociaux ou est-ce qu’ils y passent davantage de temps parce qu’ils sont malheureux ?</P>
			<P id="p48">Là est la question.</P>
			<P id="p49">Une autre étude distinguait encore l’utilisation active ou passive des réseaux sociaux.</P>
			<P id="p50">La première stimulant le capital social et menant au bonheur.</P>
			<P id="p51">La seconde favorisant la comparaison négative et ayant un impact négatif sur le bonheur.</P>
        </PAR>
        <PAR id="par8">
            <P id="p52">En attendant, des expériences d’assignations aléatoires ont démontré que des personnes qui s’étaient passé de leur smartphone pendant une semaine ou plus voyaient des effets bénéfiques sur leur bien-être.</P>
            <P id="p53">Et un cours sur le bonheur, aux États-Unis, a même connu un succès retentissant, preuve s’il en est que le sujet titille les Américains.</P>
            <P id="p54">Certains spécialistes préconisent aussi de ne pas passer plus de 30 minutes par jour sur les réseaux sociaux.</P>
			<P id="p55">Mais qui est prêt à tester l’expérience, tiens ?</P>
        </PAR>
    </TEXTE>
</DOCUMENT>
