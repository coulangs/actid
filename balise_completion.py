import re

# Lecture du texte avec balises de modification odt
with open('travail.xml', "r") as xml:
    texte = xml.read()

# Mise en mémoire de la liste de déclaration des modifications
id2modif = {}
with open('liste_modif.csv', "r") as modif:
    for line in modif:
        line = line.strip()
        l = line.split(',')
        if len(l)==7:
            i, typ, aut, dat, typ0, aut0, dat0 = l
            if i not in id2modif.keys():
                id2modif[i] = {"type":typ, "aut":aut, "date":dat, "type0":typ0, "aut0":aut0, "date0":dat0}
            else:
                print("Doublon id :",i)

def completion(s):
    if len(id2modif[s.group(1)]["type0"])>0:
        # Si il y a une substitution (type0, aut0, date0)...
        return '<edit id="{}" type="{}" aut="{}" date="{}" first-type="{}" first-aut="{}" first-date="{}">{}</edit>'.format(s.group(1), id2modif[s.group(1)]["type"], id2modif[s.group(1)]["aut"], id2modif[s.group(1)]["date"], id2modif[s.group(1)]["type0"], id2modif[s.group(1)]["aut0"], id2modif[s.group(1)]["date0"], s.group(2)) 
    else:
        # dans tous les autres cas...
        return '<edit id="{}" type="{}" aut="{}" date="{}">{}</edit>'.format(s.group(1), id2modif[s.group(1)]["type"], id2modif[s.group(1)]["aut"], id2modif[s.group(1)]["date"], s.group(2))

regex = r'<text:change-start text:change-id="(\w+)"/>(.*?)<text:change-end text:change-id="\w+"/>'

texte = re.sub(regex, lambda s:completion(s), texte)

print(texte)

with open('output.xml', 'w') as outfile:
    outfile.write(texte)